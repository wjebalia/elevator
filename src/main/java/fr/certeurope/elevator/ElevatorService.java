package fr.certeurope.elevator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;

@Service
public class ElevatorService {

    public String generateElevatorJourney(String jsonInput) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode json = objectMapper.readTree(jsonInput);

        int floor = 0;
        ArrayList<Integer> targets = new ArrayList<>();
        for (Iterator<JsonNode> it = json.elements(); it.hasNext(); ) {
            JsonNode next = it.next();
            if(targets.isEmpty() || !(targets.get(targets.size()-1) == next.get("fromFloor").asInt())) { // don't add target if it is the same as the previous one
                targets.add(next.get("fromFloor").asInt());
            }
            if(targets.isEmpty() || !(targets.get(targets.size()-1) == next.get("toFloor").asInt())) { // don't add target if it is the same as the precedent
                targets.add(next.get("toFloor").asInt());
            }
        }
        if(targets.get(0) == floor) {
            targets.remove(0);
        }

        String result = "";
        boolean move = false;
        while(true) {
            result += (move ? floor : "S") + "-";
            // Moving the elevator
            if(!targets.isEmpty()) {
                if (targets.get(0) > floor) {
                    if(move) {
                        floor++;
                    }
                    move = true;
                } else if (targets.get(0) < floor) {
                    if(move) {
                        floor--;
                    }
                    move = true;
                } else if (targets.get(0) == floor) {
                    // removing the target when reached
                    targets.remove(0);
                    move = false;
                }
            }

            // Stopping the simulation if no more calls and the elevator reached target
            if(targets.isEmpty()) {
                return result + "S";
            }
        }

    }

}
