package fr.certeurope.elevator;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ElevatorServiceTest {

    private final ElevatorService elevatorService = new ElevatorService();

    @Test
    void generationForOneCall() throws IOException {
        String json = getJson("input-example-1.json");

        String result = elevatorService.generateElevatorJourney(json);

        assertEquals("S-0-1-2-3-4-5-S", result);
    }

    @Test
    void generationForTwoConsecutiveUpwardCalls() throws IOException {
        String json = getJson("input-example-2.json");

        String result = elevatorService.generateElevatorJourney(json);

        assertEquals("S-0-1-2-3-4-5-S-5-4-3-2-1-0-S-0-1-2-3-4-5-S", result);
    }

    @Test
    void generationForOneUpwardThenOneDownwardCall() throws IOException {
        String json = getJson("input-example-3.json");

        String result = elevatorService.generateElevatorJourney(json);

        assertEquals("S-0-1-2-3-4-5-6-7-8-S-8-7-6-5-4-S-4-3-2-1-0-S-0-1-2-3-4-5-S", result);
    }

    private String getJson(String path) throws IOException {
        File resource = new ClassPathResource(path).getFile();
        return new String(Files.readAllBytes(resource.toPath()));
    }
}